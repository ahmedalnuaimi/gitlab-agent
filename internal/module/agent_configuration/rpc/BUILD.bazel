load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:proto.bzl", "go_grpc_generate")

go_grpc_generate(
    src = "rpc.proto",
    workspace_relative_target_directory = "internal/module/agent_configuration/rpc",
    deps = [
        "//internal/module/modshared:proto",
        "//pkg/agentcfg:proto",
        "@com_github_envoyproxy_protoc_gen_validate//validate:validate_proto",
    ],
)

go_library(
    name = "rpc",
    srcs = [
        "configuration_watcher.go",
        "rpc.pb.go",
        "rpc.pb.validate.go",
        "rpc_grpc.pb.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/module/agent_configuration/rpc",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/module/modshared",
        "//internal/tool/grpctool",
        "//internal/tool/logz",
        "//internal/tool/retry",
        "//pkg/agentcfg",
        "@com_github_envoyproxy_protoc_gen_validate//validate:go_custom_library",
        "@org_golang_google_grpc//:grpc",
        "@org_golang_google_grpc//codes",
        "@org_golang_google_grpc//status",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_uber_go_zap//:zap",
    ],
)

go_custom_test(
    name = "rpc_test",
    srcs = ["configuration_watcher_test.go"],
    deps = [
        ":rpc",
        "//internal/tool/testing/matcher",
        "//internal/tool/testing/mock_rpc",
        "//internal/tool/testing/testhelpers",
        "//pkg/agentcfg",
        "@com_github_golang_mock//gomock",
        "@com_github_google_go_cmp//cmp",
        "@com_github_stretchr_testify//assert",
        "@org_golang_google_protobuf//testing/protocmp",
        "@org_uber_go_zap//zaptest",
    ],
)
